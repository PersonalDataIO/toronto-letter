PersonalData.IO is a [Geneva-based nonprofit](https://www.personaldata.io/about) 
focused on making personal data rights actionable and useful. 

For this we build basic but trust-inducing tools to help:

 * individuals reappropriate their personal data
 * communities find new value out of this datahas a methodology 

We cannot be everywhere, so we have developed a methodology to help empower 
local and temporary communities around conferences, such as OpenCon. 

The setup we imagine is 5-10 people in a room, with at least one with slightly
higher technical skills. 

If you have feedback, you can enter it [here](https://hackmd.io/Bsg5X5vMTZqUy-8appV6Qw?both) or as a GitHub issue. 

Try clicking below to test our idea live on MyBinder, we hope it is sufficiently
self-explanatory:

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/PersonalDataIO%2Ftoronto-letter/master?filepath=OpenCon.ipynb)